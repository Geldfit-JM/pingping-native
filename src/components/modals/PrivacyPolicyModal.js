import React, { useEffect, useState } from 'react';

import { useMutation } from '@apollo/client';
import { Dimensions, Modal, StyleSheet, Platform, Linking, View, Text } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import checkVersion from 'react-native-store-version';

import { version } from '../../../package.json';
import GET_STATUS_QUERY from '../../apollo/Query/getStatusQuery';
import CloseIcon from '../../assets/svg/icons/CloseIcon';
import UpdateSvg from '../../assets/svg/UpdateSvg';
import { ASYNC_STORAGE_KEYS, TOA_URL } from '../../config/constants';
import theme from '../../config/theme';
import sentryHelper from '../../helpers/sentryHelper';
import RoundedButton from '../shared/RoundedButton';
import Body from '../typography/Body';
import Title from '../typography/Title';
import useAppContext from '../../hooks/useAppContext';
import ACCEPTED_PRIVACY_POLICY_MUTATION from '../../apollo/Mutation/acceptedPrivacyPolicyMutation';
import { acceptPrivacyPolicy } from '../../helpers/questionAnswerHelpers';

const screenWidth = Dimensions.get('window').width;

function PrivacyPolicyModal() {
	const [open, setOpen] = useState(false);
	const { acceptedPrivacyPolicy, setAcceptedPrivacyPolicy } = useAppContext();
	const [accepted, setAccepted] = useState(false);
	const [acceptedPrivacyPolicyMutation] = useMutation(ACCEPTED_PRIVACY_POLICY_MUTATION);
	const appctx = useAppContext();

	useEffect(() => {
		const init = () => {
			if (!acceptedPrivacyPolicy)
				setOpen(true);
		};
		init();
	}, []);

	const accept = async () => {
		try {
			await acceptPrivacyPolicy(acceptedPrivacyPolicyMutation, setAcceptedPrivacyPolicy);
			setOpen(false);
		} catch (error) {
			sentryHelper(error.message);
		}
	}

	const onLinkClick = async () => {
		Linking.openURL("https://geldzorgenvrij.nl/privacy-policy/");
	}

	return (
		<Modal animationType="fade" transparent visible={!acceptedPrivacyPolicy} statusBarTranslucent>
			<View style={styles.centeredView}>
				<View style={styles.modalView}>
					<View style={styles.modalContainer}>
						<Title variant="h3" style={styles.title}>
							Bijgewerkte privacyverklaring
						</Title>
						<View style={styles.svgContainer}>
							<UpdateSvg width={91} height={94} />
						</View>
						<Body variant="b3" style={styles.body}>
							Hi, Wat mooi dat jij PingPing gebruikt! De app is overgedragen naar stichting NL Geldzorgenvrij. Natuurlijk blijven jouw gegevens veilig.
						</Body>
						<Body variant="b3" style={{ ...styles.body, ...styles.link }} onPress={onLinkClick}>
							Lees hier mee over in ons aangepaste privacy statement.
						</Body>
						<View style={styles.checkboxContainer}>
							<CheckBox
								value={accepted}
								onValueChange={setAccepted}
								style={styles.checkbox}
							/>
							<Text style={styles.label}>Ik heb de privacyverklaring gelezen en geaccepteerd</Text>
						</View>
						<RoundedButton label="Bevestigen" disabled={!accepted} full onPress={accept} />
					</View>
				</View>
			</View>
		</Modal>
	);
}

const styles = StyleSheet.create({
	centeredView: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: theme.colors.modalBackground,
	},
	modalView: {
		width: screenWidth * 0.9,
		backgroundColor: theme.colors.white,
		borderRadius: 5,
		shadowColor: theme.colors.black,
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5,
	},
	modalContainer: {
		paddingHorizontal: theme.spacing.xl,
		paddingVertical: theme.spacing.m,
		position: 'relative',
	},
	closeButton: {
		borderRadius: 50,
		position: 'absolute',
		right: 0,
	},
	svgContainer: {
		alignSelf: 'center',
	},
	icon: {
		color: theme.colors.black,
		margin: theme.spacing.xs,
	},
	title: {
		marginBottom: theme.spacing.l,
	},
	body: {
		marginVertical: theme.spacing.l,
	},
	link: {
		marginTop: 0,
		color: "blue",
	},
	checkboxContainer: {
		flexDirection: "row",
		alignItems: "center",
		marginBottom: theme.spacing.l,
		justifyContent: "center",
	}
});

export default PrivacyPolicyModal;
