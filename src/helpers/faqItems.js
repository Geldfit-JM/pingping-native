import { createElement } from 'react';
import { Linking, Text } from 'react-native';
import Body from '../components/typography/Body';

export const faqItems = [
	{
		question: 'Welke gegevens van mij gebruiken jullie voor het advies over mijn route?',
		answer: 'Je geboortedatum, of je een vast adres hebt, of je een bankrekening hebt, of je een DigiD hebt, of je een Zorgverzekering hebt, of je Zorgtoeslag hebt, of je staat ingeschreven bij Woningnet. Dit slaan we op middels een code, die niet te herleiden is tot jou. ',
	},
	{
		question: 'Waarom slaan jullie mijn gegevens op?',
		answer: 'Omdat dit makkelijker is voor jou om de app te gebruiken. Dan hoef je niet elke keer alles opnieuw in te vullen.',
	},
	{
		question: 'Waar slaan jullie mijn gegevens op?',
		answer: 'We slaan jouw gegevens veilig op bij het datapunt van Stichting Nederland Geldzorgenvrij. PingPing is inmiddels een activiteit van [Stichting Nederland Geldzorgenvrij](https://geldzorgenvrij.nl/) ',
	},
	{
		question: 'Wat gebeurt er verder met die gegevens?',
		answer: 'Helemaal niets. Als je de app niet meer gebruikt wissen we jouw gegevens na 3 maanden.',
	},
	{
		question: 'Kan ik mijn gegevens wijzigen of verwijderen?',
		answer: 'Ja, dat kan in de app PingPing, voor andere diensten kijk voor meer informatie op deze link: [Stichting Nederland Geldzorgenvrij](https://geldzorgenvrij.nl/). ',
	},
	{
		question: 'Wie kan ik bellen met vragen over mijn gegevens?',
		answer: 'PingPing is inmiddels een activiteit van Stichting Nederland Geldzorgenvrij, voor meer informatie kijk op [Nederland Geldzorgenvrij](https://geldzorgenvrij.nl/) ',
	},
	{
		question: 'TADA? Wat is dat?',
		answer: 'Wij passen zover technisch mogelijk, de 6 tada principes toe. Klik op deze link voor meer informatie: tada.city ',
	},
];

export function createFAQItemElement(item, linkStyle) {
	let parts = [];
	let anchor = 0;
	while (true) {
		let nextLink = item.indexOf("[", anchor);
		if (nextLink == -1)
			break;

		// Save text so far
		parts.push(item.substring(anchor, nextLink));

		// Extract link
		anchor = nextLink + 1;
		let end = item.indexOf("]", anchor);
		let text = item.substring(anchor, end);
		anchor = end + 2;
		end = item.indexOf(")", anchor);
		let link = item.substring(anchor, end);
		let linkElement = createElement(Text, {
			key: anchor,
			style: linkStyle,
			onPress: function() { Linking.openURL(link) }
		}, text);
		parts.push(linkElement);
		anchor = end + 1;
	}
	parts.push(item.substring(anchor));
	return parts;
}


