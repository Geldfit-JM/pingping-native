import { gql } from '@apollo/client';

const ACCEPTED_PRIVACY_POLICY_MUTATION = gql`
	mutation ACCEPTED_PRIVACY_POLICY_MUTATION(
		$status: Boolean
	) {
		acceptedPrivacyPolicy(status: $status)
	}
`;

export default ACCEPTED_PRIVACY_POLICY_MUTATION;
