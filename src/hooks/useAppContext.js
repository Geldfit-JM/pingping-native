import { useContext } from 'react';

import { AppContext } from '../App/AppContext';

function useAppContext() {
	const {
		userState,
		bootIssue,
		acceptedPrivacyPolicy,
		setUserState,
		setAcceptedPrivacyPolicy,
		retry,
	} = useContext(AppContext);

	return {
		userState,
		bootIssue,
		acceptedPrivacyPolicy,
		setUserState,
		setAcceptedPrivacyPolicy,
		retry,
	};
}

export default useAppContext;
